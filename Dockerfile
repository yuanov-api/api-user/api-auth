FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9-slim

COPY . /app
RUN pip install -q -r requirements.txt
CMD ["bash", "./entrypoint.sh"]
