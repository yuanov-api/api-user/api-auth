from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app import settings
from app.routers.user import router as user_router

tags_metadata = [
]

app = FastAPI(debug=settings.DEBUG, openapi_tags=tags_metadata)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(user_router, tags=['user'])
