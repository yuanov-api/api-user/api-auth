from datetime import datetime
from typing import Any
from typing import Optional

from humps import camelize
from pydantic import BaseModel
from pydantic import UUID4
from pydantic.main import ModelMetaclass


class ApiModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True


class AllOptional(ModelMetaclass):
    def __new__(mcs, name, bases, namespaces, **kwargs):
        annotations = namespaces.get('__annotations__', {})
        for base in bases:
            annotations.update(base.__annotations__)
        for field in annotations:
            if not field.startswith('__'):
                annotations[field] = Optional[annotations[field]]
        namespaces['__annotations__'] = annotations
        return super().__new__(mcs, name, bases, namespaces, **kwargs)


class IdModel(ApiModel):
    id: UUID4


class UsageInfoModel(ApiModel):
    created: datetime
    updated: datetime
    created_by: UUID4
    updated_by: UUID4

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.updated = self.updated.replace(microsecond=0)
        self.created = self.created.replace(microsecond=0)


class GetModel(UsageInfoModel, IdModel):
    pass
