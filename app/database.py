import uuid
from copy import deepcopy

from sqlalchemy import Column, Integer, String, func, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app import settings

engine = create_async_engine(settings.DB_URL, future=True, echo=False)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session


class User(Base):
    __tablename__ = "filter"

    id = Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True, index=True, unique=True)
    is_deleted = Column(Integer, default=0)

    created_at = Column(DateTime(timezone=True),
                        server_default=func.now(),
                        nullable=False)
    updated_at = Column(DateTime(timezone=True),
                        server_default=func.now(),
                        onupdate=func.now(),
                        nullable=False)
    created_by = Column(UUID(as_uuid=True), nullable=False)
    updated_by = Column(UUID(as_uuid=True), nullable=False)

    first_name = Column(String, nullable=True)
    last_name = Column(String, nullable=True)
    ordering = Column(String, nullable=True)
    description = Column(String)
    name = Column(String)

    @property
    def api_dict(self):
        api_dict = deepcopy(self.__dict__)

        api_dict.pop('guid')
        api_dict.pop('is_deleted')

        api_dict['id'] = str(self.guid)

        return api_dict
